// Función Calcular
function calcular(){
    valorAuto = document.getElementById('txtValor').value;
    enganche = document.getElementById('txtEnganche');
    totalFinanciar = document.getElementById('txtFinanciar');
    pagoCredito = document.getElementById('cmbPlanes').value;
    pagoMensual = document.getElementById('txtPago');

    enganche.innerHTML = "$" + (valorAuto*0.30);
 
    // Variable temporal para tener valorAuto - enganche
    temp = (valorAuto - (valorAuto*0.30));

    switch((pagoCredito*1)){
        case 12:
            totalFinanciar.innerHTML = "$" + (temp*1.125);
            pagoMensual.innerHTML = "$" + (temp*1.125 / pagoCredito).toFixed(2);
            break;
        case 18:
            totalFinanciar.innerHTML = "$" + (temp*1.172);
            pagoMensual.innerHTML = "$" + (temp*1.172 / pagoCredito).toFixed(2);
            break;
        case 24:
            totalFinanciar.innerHTML = "$" + (temp*1.21);
            pagoMensual.innerHTML = "$" + (temp*1.21 / pagoCredito).toFixed(2);
            break;
        case 36:
            totalFinanciar.innerHTML = "$" + (temp*1.26);
            pagoMensual.innerHTML = "$" + (temp*1.26 / pagoCredito).toFixed(2);
            break;
        case 48:
            totalFinanciar.innerHTML = "$" + (temp*1.45);
            pagoMensual.innerHTML = "$" + (temp*1.45 / pagoCredito).toFixed(2);
            break;
    }

    

}

// Función Limpiar
function limpiar(){
    valorAuto = document.getElementById('txtValor');
    enganche = document.getElementById('txtEnganche');
    totalFinanciar = document.getElementById('txtFinanciar');
    pagoMensual = document.getElementById('txtPago');

    valorAuto.value = "";
    enganche.innerHTML = "";
    totalFinanciar.innerHTML = "";
    pagoMensual.innerHTML = "";

}
